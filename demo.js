/**
 * Created by DoubleHappyi on 2014/4/10.
 */

(function($){
    $(function(){

        //swiper
        var windowHeight = $(window).height();
        var $swiperWrapper = $(".swiper-wrapper");
        $swiperWrapper.height(windowHeight);
        var mySwiper = new Swiper('.swiper-container',{
            mode:'vertical',
            loop: false,
            initialSlide:1,
            autoplay:false,
            onTouchEnd : function() {
                //Do something when you touch the slide
                console.log('onTouchEnd...');
                mySwiper.swipeTo(1);
                return false
            }
        });


        //iscroll
        var myIScroll = new IScroll("#iscroll-container",{
            scrollX: true,
            scrollY: false
        });

    });
})(Zepto);